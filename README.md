# Digito Unico

O projeto consiste em um cálculo de dígito único dado ``n`` e ``k`` sendo inteiros 
e ``n = valor`` e ``k = quantidade de concatenacao do valor``

Exemplo: ``n = 23`` e ``k = 2``
Logo a concatenação é igual a ``2323``

Tendo então o número concatenado, o cálculo de dígito único se dá da seguinte forma:
``2+3+2+3`` que é igual ao resultado ``10``, logo o dígito único de ``2323 = 10``

___
### Clone e Start da aplicação

Abrir o Prompt de Comando no diretório desejado e digitar:

``git clone https://gitlab.com/contato.gabriela/digito-unico.git``

Abrir o conteúdo baixado na IDE de sua preferência, ao fazer o build as dependências serão baixadas 
e é só dar um start run em ``DigitoUnicoApplication (main) do projeto`` 

___
### Swagger

Após startado a aplicação para acessar a documentação dos endpoints disponíveis 
basta acessar ``http://localhost:8090/api/swagger-ui/index.html``

___
### H2Database

Como base de dados foi utilizado um banco em memória, e é possível acessá-lo 
pelo endereço: ``http://localhost:8090/api/h2-console/login.do`` com user: ``sa`` e senha: ``123``

___
### Colletions do Postman

A collection do Postman está na raiz do projeto com os endpoints e as respostas exemplo, nomeado como postman_collection.json 
Basta importá-lo no Postman. 

___
### Chave de Criptografia

No init da aplicação é criada uma chavce pública e privada em c:/keys para serem utilizadas na cri´tografia do nome

A criptografia não está sendo revertida até o momento. 

___
### Testes Unitários 
Alguns testes foram realizados e para testá-los basta do lado direto do mouse em cima do diretório de testes, ou da aplicação, e dar um "Run All Tests".


