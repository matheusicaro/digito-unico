package br.com.inter.digitounico.service;

import br.com.inter.digitounico.model.dto.ResultDTO;
import br.com.inter.digitounico.model.entity.Result;
import br.com.inter.digitounico.models.Entities;
import br.com.inter.digitounico.repository.ResultRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
public class ResultServiceTest {

    @MockBean
    private ResultService resultService;

    @Mock
    private ResultRepository resultRepository;

    @Test
    public void convertDTO(){
        ResultDTO resultDTO = Entities.getResultDTO();
        Result result = Entities.getResultEntity();
        result.setId(null);
        when(resultService.convertDTO(resultDTO)).thenReturn(result);
        Result response = resultService.convertDTO(resultDTO);

        Assert.assertEquals(result, response);
    }

    @Test
    public void retornarNumeroConcatenado(){
        Integer n = 1;
        Integer qtdeConcatenacao = 2;
        Integer number = 11;

        when(resultService.retornarNumeroConcatenado(n, qtdeConcatenacao)).thenReturn(number);
        Integer response = resultService.retornarNumeroConcatenado(n, qtdeConcatenacao);

        Assert.assertEquals(number, response);
        Assert.assertNotNull(response);
    }

    @Test
    public void retornarDigitoUnico(){
        Integer number = 11;
        Integer expected = 2;

        when(resultService.retornarDigitoUnico(number)).thenReturn(expected);
        Integer response = resultService.retornarDigitoUnico(number);

        Assert.assertNotNull(response);
        Assert.assertTrue(response.equals(2));
        Assert.assertEquals(expected, response);
    }

    @Test
    public void last10Results(){
        Result result = Entities.getResultEntity();
        List<Result> resultList = new ArrayList<>();
        resultList.add(result);

        int qtde = 10;

        when(resultRepository.findLast10Results(qtde)).thenReturn(resultList);
        when(resultService.last10Results(qtde)).thenReturn(resultList);
        List<Result> results =  resultService.last10Results(qtde);

        Assert.assertEquals(results, resultList);
        Assert.assertTrue(results.size() > 0);
    }
}
