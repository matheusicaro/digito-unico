package br.com.inter.digitounico.controller;

import br.com.inter.digitounico.model.dto.UserDTO;
import br.com.inter.digitounico.model.entity.User;
import br.com.inter.digitounico.models.Entities;
import br.com.inter.digitounico.service.UserService;
import ch.qos.logback.core.net.ObjectWriter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONUtil;
import nonapi.io.github.classgraph.json.JSONUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import springfox.documentation.spring.web.json.Json;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    void get() throws Exception {
        User user = Entities.getUser();
        String id = "teste_id";

        when(userService.findById(any())).thenReturn(user);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

    }

    @Test
    void create() throws Exception {
        User user = Entities.getUser();
        UserDTO userDTO = Entities.getUserDTO();

        when(userService.create(userDTO)).thenReturn(user);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.post("/users/create")
        .contentType(MediaType.APPLICATION_JSON)
        .content(mapper.writeValueAsString(user))).andExpect(status().isOk());
    }
}
