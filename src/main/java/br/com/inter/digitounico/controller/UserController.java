package br.com.inter.digitounico.controller;

import br.com.inter.digitounico.model.dto.UserDTO;
import br.com.inter.digitounico.model.entity.User;
import br.com.inter.digitounico.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Optional;

@Api(value = "User")
@RestController
@RequestMapping(path = "/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Busca de usuário", notes = "Endpoint responsável por buscar usuário pelo id")
    public ResponseEntity<User> getById(@PathVariable String id) throws IOException, ClassNotFoundException {
        return ResponseEntity.ok(userService.findById(id));
    }

    @PostMapping("/create")
    @ApiOperation(value = "Criação de Usuário", notes = "Endpoint responsável pela criação de usuário")
    public ResponseEntity<User> create(@RequestBody UserDTO userDTO) throws IOException, ClassNotFoundException {
        return ResponseEntity.ok(this.userService.create(userDTO));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deletando usuário", notes = "Endpoint responsável por deletar usuário na base")
    public ResponseEntity create(@PathVariable String id) throws IOException {
        userService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
