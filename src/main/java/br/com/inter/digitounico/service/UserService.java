package br.com.inter.digitounico.service;

import br.com.inter.digitounico.model.dto.UserDTO;
import br.com.inter.digitounico.model.entity.User;
import br.com.inter.digitounico.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.PublicKey;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CriptografiaService criptografiaService;

    /**
     * Local da chave privada no sistema de arquivos.
     */
    public static final String PATH_CHAVE_PRIVADA = "C:/keys/private.key";

    /**
     * Local da chave pública no sistema de arquivos.
     */
    public static final String PATH_CHAVE_PUBLICA = "C:/keys/public.key";

    public User create(UserDTO userDTO) throws IOException, ClassNotFoundException {
        return userRepository.save(convertDTO(userDTO));
    }

    @Transactional
    public User findById(String id) throws IOException, ClassNotFoundException {
        return userRepository.findById(id).get();

//        if(user != null){
//            ObjectInputStream inputStream = null;
//            inputStream = new ObjectInputStream(new FileInputStream(PATH_CHAVE_PUBLICA));
//            final PublicKey chavePrivada = (PublicKey) inputStream.readObject();
//
//            String name = criptografiaService.decriptografa(user.getName().get, chavePrivada);
//
//            //UserDTO userDTO = convertEntity(name, user.getEmail());
//        }
    }

    public User save(User user){
        return userRepository.save(user);
    }

    public User convertDTO(UserDTO userDTO) throws IOException, ClassNotFoundException {
        User user = new User();
        ObjectInputStream inputStream = null;
        inputStream = new ObjectInputStream(new FileInputStream(PATH_CHAVE_PUBLICA));
        final PublicKey chavePublica = (PublicKey) inputStream.readObject();
        user.setName(criptografiaService.criptografa(userDTO.getName(), chavePublica).toString());
        user.setEmail(userDTO.getEmail());

        return user;
    }


    public void delete(String id){
        User user = userRepository.getOne(id);
        if(user != null){
            userRepository.delete(user);
        }
    }
}
